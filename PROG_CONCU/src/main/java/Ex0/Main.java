package Ex0;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// TODO Auto-generated method stub
		System.out.println("Le thread courrant est : "+Thread.currentThread().getName());
		
		Runnable task1 = 
				() -> System.out.printf("[TASK] Je suis dans le thread [%s]\n", 
						Thread.currentThread().getName());
		//task.run();
		//Thread thread = new Thread(task);
		//thread.start();
		Callable<String> task2 = 
						() -> "[TASK] Je suis dans le thread " +
								Thread.currentThread().getName();		
				
				ExecutorService executorService = 
						Executors.newFixedThreadPool(3);
				
				Future<?> f1 = executorService.submit(task1);
				Future<String> f2 = executorService.submit(task2);
				Future<String> f3 = executorService.submit(task2);
				Future<String> f4 = executorService.submit(task2);
				Future<String> f5 = executorService.submit(task2);
				Future<String> f6 = executorService.submit(task2);
				
				List<Future<?>> futures = List.of(f1, f2, f3, f4, f5, f6);
				for (Future<?> f : futures) {
					System.out.println(f.get());
				}
				
				executorService.shutdown();
		
		
	}

}
