package Ex1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Compteur compteur=new Compteur();
		
		Runnable taskInc = 
				() -> {compteur.increment();};
				
		ExecutorService executorService = 
						Executors.newFixedThreadPool(10);
		
		for (int i = 0; i < 10; i++) {
			executorService.submit(taskInc);
			executorService.submit(taskInc);
			executorService.submit(taskInc);
			executorService.submit(taskInc);
			executorService.submit(taskInc);
			executorService.submit(taskInc);
			executorService.submit(taskInc);
			executorService.submit(taskInc);
			executorService.submit(taskInc);
			executorService.submit(taskInc);
		}
		
		
		
		
		
	}

}
