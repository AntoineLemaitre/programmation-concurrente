package Ex2;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class WareHouse {

	private static final int capacity=7;
	private int level=0;
	private Lock lock = new ReentrantLock();
	private Condition notFull = lock.newCondition();
	private Condition notEmpty = lock.newCondition();

	public int getCapacity() {
		return capacity;
	}
	
	public void add() throws InterruptedException {
		lock.lock();
		try {

			while (level == capacity) {
				notFull.await();
			}
			level++;
			notEmpty.signalAll();
			
		} finally {
			lock.unlock();
		}
	}
	public synchronized void remove() throws InterruptedException {
		lock.lock();
		try {

			while (level== 0) {
				notEmpty.await();
			}
			level--;
			notFull.signalAll();
			
		} finally {
			lock.unlock();
		}
	}
	public int content() {
		return level;
		
	}
}
