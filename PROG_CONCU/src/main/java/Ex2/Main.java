package Ex2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		WareHouse cave=new WareHouse();
		Runnable taskInc = 
				() -> {try {
					cave.add();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}};
		Runnable taskDec = 
						() -> {try {
							cave.remove();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}};		
		ExecutorService executorService = 
						Executors.newFixedThreadPool(5);
		for (int i = 0; i < 100; i++) {
			executorService.submit(taskInc);
			//System.out.println("La cave contient : "+cave.content());
		}
		for (int i = 0; i < 95; i++) {
			executorService.submit(taskDec);
			//System.out.println("La cave contient : "+cave.content());
		}
		System.out.println("La cave contient : "+cave.content());
		
		
		
		
	}

}
