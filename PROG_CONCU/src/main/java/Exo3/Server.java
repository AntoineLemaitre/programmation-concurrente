package Exo3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

	static final Random rand = new Random();
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		ConcurrentHashMap<Long, Product> map = 
				new ConcurrentHashMap<>();
		
		//Maximum 5 clients en même temps les autres en attentes
		ExecutorService service = Executors.newFixedThreadPool(5) ;		
		
		String threadName = Thread.currentThread().getName();
		ServerSocket server = new ServerSocket(8080);

		while (true) {
			System.out.println("[" + threadName + "] Listening to request");
			Socket socket = server.accept();

			Runnable task = () -> serveClient(socket,map);
			service.submit(task);
			
			
		}
	}

	private static void serveClient(Socket socket,ConcurrentHashMap<Long, Product> map) {
		String threadName = Thread.currentThread().getName();
		try {
			System.out.println("[" + threadName + "] Accepting request");

			InputStream inputStream = socket.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			BufferedReader reader = new BufferedReader(inputStreamReader);

			OutputStream outputStream = socket.getOutputStream();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
			PrintWriter writer = new PrintWriter(outputStreamWriter);
			
			
			String order = reader.readLine();
			
			while (order != null) {
				if (order.startsWith("GET")) {
					writer.printf("[%s] Received GET order : %s\n", threadName, order);
					writer.flush();
					System.out.printf("[%s] Received GET order : %s\n", threadName, order);
					order = reader.readLine();
				} 
				else if(order.equals("LIST"))
				{
					System.out.printf("[%s] Received LIST order : %s\n", threadName, order);
					if(map.isEmpty())
					{
						writer.printf("No product\n");
					}
					else
					{
						for (Entry<Long, Product> e : map.entrySet()) {
							writer.printf((e.getKey()+" "+e.getValue().getName()+" "+e.getValue().getPrice())+"\n");
					    }
						
					}
					
				
					writer.flush();
					
					//writer.printf(k+" "+v);writer.flush();
					
					order = reader.readLine();
				}
				else if(order.startsWith("CREATE"))
				{
					if(order.split(" ").length == 3)
					{
						String[] parametre = order.split(" ");
						long key = rand.nextLong();
						Product produit = new Product(key);
						produit.setName(parametre[1]);
						produit.setPrice(Integer.valueOf(parametre[2]));
						
						map.put(key, produit);
						
						writer.printf("[%s] CREATED Id : %d\n", threadName, key);
						
						System.out.printf("[%s] Received CREATE order : %s\n", threadName, order);
						
						System.out.println("Contenu actuel de notre map");
						
						map.forEach((k,v) ->System.out.println(k+"--->"+v.getName()+" "+v.getPrice()));
						
						writer.flush();
						order = reader.readLine();
							
					}
					else
					{
						writer.printf("[%s] Bad CREATE request : %s\n", threadName, order);
						writer.flush();
						System.out.printf("[%s] Bad CREATE request : %s\n", threadName, order);
						order = reader.readLine();
					}
					
				}
				
				else if(order.startsWith("BUY"))
				{
					if(order.split(" ").length == 2)
					{
						String[] parametre = order.split(" ");
						
						System.out.println("on est dans le bloc");
						if( map.containsKey(Long.valueOf(parametre[1])))
						{
							System.out.println("avant map remove");
							Product produit = map.remove(Long.valueOf(parametre[1]));
							writer.printf("[%s] Received and Executed BUY %d\n", threadName, produit.getId());
							System.out.printf("[%s] Received and Executed BUY %d\n", threadName, produit.getId());
							System.out.println("après map remove");
						}
						else
						{
							writer.printf("[%s] Received and Not Executed BUY %d\n", threadName, Integer.valueOf(parametre[1]));
							System.out.printf("[%s] Received and Not Executed BUY %d\n", threadName, Integer.valueOf(parametre[1]));
						}
						
						
						System.out.println("Contenu actuel de notre map");
						
						map.forEach((k,v) ->System.out.println(k+"--->"+v));
						
						writer.flush();
						order = reader.readLine();
					}
					else
					{
						writer.printf("[%s] Bad BUY request : %s\n", threadName, order);
						writer.flush();
						System.out.printf("[%s] Bad BUY request : %s\n", threadName, order);
						order = reader.readLine();
					}
				}
				else if (order.equals("quit")) {
					System.out.printf("[" + threadName + "] Closing connection\n");
					socket.close();
					order = null;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
