package Exo3;

public class Product {

	private long id;
	private String name;
	private int price;
	
	
	Product()
	{

	}
	Product(long id)
	{
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public long getId() {
		return id;
	}
}
